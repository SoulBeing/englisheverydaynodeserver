/*eslint-env node */
var express = require('express');
var bodyParser = require('body-parser');
var PouchDB = require('pouchdb');

module.exports.router = function () {
	var allAdviceLlearningItems = [];
	var _db = PouchDB('advice_learningitem_everyday');
	var remote = 'https://e01b7b37-774e-408f-bb5a-9bea19a224cf-bluemix.cloudant.com/advice_learningitem_everyday/';
	var options = {
		live: true,
		retry: true,
		continuous: true,
		auth: {
			username: 'e01b7b37-774e-408f-bb5a-9bea19a224cf-bluemix',
			password: 'f2476f9abfbc86add8f480f945f5a75277cefb3f897655e5a2945a7ef6b6e081'
		}
	};
	_db.sync(remote, options);

	// have to return router always
	return express.Router()
		.use(bodyParser.json())
		.get('/', doGet)
		.post('/', doPost)
		.post('/refresh', refresh);

	function doGet(req, res) {
		var targetDate = req.query.date;
		var dataOfTargetDate = getDataOfDate(targetDate);
//		if(dataOfTargetDate){
//			res.status(200).json(dataOfTargetDate);
//		}else{
//			_db.get(targetDate)  // date is the specific doc's id;
//			.then((result) => {
//				var dataOfDate = {
//					date: targetDate,
//					data: result.data
//				};
//				allAdviceLlearningItems.push(dataOfDate);
//				return dataOfDate;
//			}).catch((error) => {
//				if(error.name === "not_found"){
//					var endOfAllData = {
//						date: targetDate,
//						data: "no_more_data"
//					}
//					allAdviceLlearningItems.push(endOfAllData);
//					res.status(200).json(endOfAllData);
//				}
//			}).then(function (result) {
//				res.status(200).json(result);
//			});
//		}
		var makeStingsbacktoWorkJson = {
			date: targetDate,
			data: {
			    "报纸难度": [
			      {
			        "imagesrc": "https://static.pexels.com/photos/157/person-apple-laptop-notebook-medium.jpg",
			        "sentence": "Our stories are reflected in their songs, and in their three-decade long journeys of ups and downs, which have forever etched their way deep into our hearts.",
			        "possibleTags": [
			          "Metro",
			          "深入人心",
			          "歌曲乐队",
			          ""
			        ]
			      },
			      {
			        "imagesrc": "https://static.pexels.com/photos/1778/numbers-time-watch-white-medium.jpg",
			        "sentence": "The lyrics \"they shot a movie once, in my hometown\" has always evoked an emotional response in me.",
			        "possibleTags": [
			          "Metro",
			          "触动",
			          "歌曲乐队",
			          ""
			        ]
			      },
			      {
			        "imagesrc": "https://static.pexels.com/photos/989/fashion-man-person-hand-medium.jpg",
			        "sentence": "It's guaranteed that I will get up and dance if i'm in a bar that starts blasting the iconic song from the 1988 album.",
			        "possibleTags": [
			          "Metro",
			          "一定会",
			          "歌曲乐队",
			          ""
			        ]
			      }
			    ]
			  }
		};
		res.status(200).json(makeStingsbacktoWorkJson);

	}
	function getDataOfDate(date) {
		return allAdviceLlearningItems.find((each) => {
			return each.date === date;
		});
	}

	function doPost(req, res) {
		var password = req.body.password;
		var date = req.body.date;
		var adviceLearningItems = req.body.data;
		if (password === 'Soul870713jx') {
			_db.get(date).then(function (doc) {
				var exsitingData = doc.data;
				var appengdingChannelName = Object.keys(adviceLearningItems)[0];
				var appengdingChannelData = adviceLearningItems[appengdingChannelName];
				exsitingData[appengdingChannelName] = appengdingChannelData;
				return _db.put(doc);
			}).then(function (response) {
				res.status(200).end();
			}).catch(function (err) {
				if(err && err.name === "not_found"){
					 _db.put({
						"_id": date,
						"data": adviceLearningItems,
					});
				}
				res.status(404).end(err.message);
			});
		}
	}
	
	function refresh(req, res){
		var password = req.body.password;
		if (password === 'Soul870713jx') {
			allAdviceLlearningItems = [];
			res.status(200).end('cache refreshed');
		}else{
			res.status(404).end('wrong password');
		}
	}
};