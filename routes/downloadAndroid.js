/*eslint-env node */
var express = require('express');
var path = require('path');

module.exports.router = function () {

	return express.Router()
	.get('/', downloadAndroid);

	function downloadAndroid(req, res){
	  var file = path.join(__dirname, '../androidApk/englisheveryday.apk');
	  res.download(file);
	}
};