/*eslint-env node */
var express = require('express');
var bodyParser = require('body-parser');
var PouchDB = require('pouchdb');

module.exports.router = function () {
	var _db = PouchDB('cloud_backup');
	var remote = 'https://e01b7b37-774e-408f-bb5a-9bea19a224cf-bluemix.cloudant.com/cloud_backup/';
	var options = {
		live: true,
		retry: true,
		continuous: true,
		auth: {
			username: 'e01b7b37-774e-408f-bb5a-9bea19a224cf-bluemix',
			password: 'f2476f9abfbc86add8f480f945f5a75277cefb3f897655e5a2945a7ef6b6e081'
		}
	};
	_db.sync(remote, options);

	// have to return router always
	return express.Router()
		.use(bodyParser.json())
		.get('/', doGet)
		.post('/', doPost);

	function doGet(req, res) {
		if (isUserVIP(req.query.userId)) {
			_db.get(req.query.userId)  // username is the specific doc's id;
				.then((result) => {
					return result.data;
				}).catch((error) => {
					console.log(error);
				}).then(function (result) {
					res.status(200).json(result);
				});
		}
	}

	function doPost(req, res) {
		var userID = req.query.userId;
		if (isUserVIP(userID)) {
			_db.get(userID).then(function (doc) {
				return _db.remove(doc);
			}).then(function (result) {
				return result;
			}).catch(function (err) {
				if (err.message === 'missing') {
					return;
				}
			}).then(function () {
				var wholeDataOfUser = req.body.data;
				return _db.put({
					"_id": userID,
					"data": wholeDataOfUser,
				}).then(function (response) {
					res.status(200).end('ok');
				}).catch(function (err) {
					res.status(404).end(err.message);
				});
			})
		}
	}
	function isUserVIP(username) {
		return true;
	}
}