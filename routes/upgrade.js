/*eslint-env node */
var express = require('express');
var path = require('path');

module.exports.router = function () {
	var NEW_VERSION_ANDROID = 10;
	var NEW_ANDROID_CHANGES_EN = "Enhanced the home page, added pronunciation";
	var NEW_ANDROID_CHANGES_CN = "强化了资源的学习性，添加了发音";
    var NEW_VERSION_IOS = 4;
	var NEW_IOS_CHANGES_EN = "Enhanced the home page, added pronunciation";
	var NEW_IOS_CHANGES_CN = "强化了资源的学习性，添加了发音";

	return express.Router()
	.get('/:platform/', getUpgradeInfo);

	function getUpgradeInfo(req, res){
		var platfrom = req.params.platform;
		var versionInPhone = req.query.version_self;

		var respondJson = {};
		if(platfrom === "android" && NEW_VERSION_ANDROID > versionInPhone){
			respondJson = {
				"hasNewVersion" : true,
				"message_en": NEW_ANDROID_CHANGES_EN,
				"message_cn": NEW_ANDROID_CHANGES_CN,
			};
		}else if(platfrom === "ios" && NEW_VERSION_IOS > versionInPhone){
			respondJson = {
				"hasNewVersion" : true,
				"message_en": NEW_IOS_CHANGES_EN,
				"message_cn": NEW_IOS_CHANGES_CN,
			};
		}
		res.status(200).json(respondJson);
	}
};