/*eslint-env node */
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var dotenv = require('dotenv');
var jwt = require('express-jwt');
var cors = require('cors');
var https = require('https');
var http = require('http');

var app = express();
dotenv.load();

var authenticate = jwt({
  secret: new Buffer(process.env.AUTH0_CLIENT_SECRET, 'base64'),
  audience: process.env.AUTH0_CLIENT_ID
});

//add comment123
app.use(cors());

// uncomment after placing your favicon in /public123create apr
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/',express.static(path.join(__dirname, 'public')));
app.use('/img',express.static(path.join(__dirname, 'img')));
app.use('/private',express.static(path.join(__dirname, 'private')));
app.use('/secured', authenticate);
app.use('/secured/cloudbackup', require('./routes/cloudbackup').router());
app.use('/secured/vipuser', require('./routes/vipUser'));
app.use('/learningitem', require('./routes/adviceLearningItem').router());
app.use('/downloadandroid',require('./routes/downloadAndroid').router());
app.use('/upgrade', require('./routes/upgrade').router());

// Get IP of the Cloud Foundry DEA (Droplet Execution Agent) that hosts this application
var host = process.env.VCAP_APP_HOST || 'localhost';
// Get the port on the DEA for communication with the application
var port = process.env.VCAP_APP_PORT || 3000;
var server;
if(process.env.SSLKEY && process.env.SSLCERT){
  var options = {
    key : process.env.SSLKEY,
    cert : process.env.SSLCERT
  };
  server = https.createServer(options, app);
}else{
  server = http.createServer(app);
}
server.listen(port, host, function (err) {
  console.log(host + port);
});

module.exports = app;
