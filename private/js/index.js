/*eslint-env browser */
function insertAdviceLeanringItem() {
    var date = document.getElementById('date').value;
    var channel = document.getElementById('channel').value;
    var sentence1 = document.getElementById('sentence1').value.trim();
    var url1 = document.getElementById('url1').value;
    var tags1 = [
        document.getElementById('tags1resource').value.trim(),
        document.getElementById('tags1theme').value.trim(),
        document.getElementById('tags1content').value.trim(),
        document.getElementById('tags1others').value.trim(),
    ];
    // Must follow this order!!

    var sentence2 = document.getElementById('sentence2').value.trim();
    var url2 = document.getElementById('url2').value;
    var tags2 = [
        document.getElementById('tags2resource').value.trim(),
        document.getElementById('tags2theme').value.trim(),
        document.getElementById('tags2content').value.trim(),
        document.getElementById('tags2others').value.trim(),
    ];

    var sentence3 = document.getElementById('sentence3').value.trim();
    var url3 = document.getElementById('url3').value;
    var tags3 = [
        document.getElementById('tags3resource').value.trim(),
        document.getElementById('tags3theme').value.trim(),
        document.getElementById('tags3content').value.trim(),
        document.getElementById('tags3others').value.trim(),
    ];
    var password = document.getElementById('key').value;

     var url = "https://englisheveryday.mybluemix.net/learningitem";
    // var url = "http://localhost:3000/learningitem";
    var method = "POST";
    var async = true;
    var channelData = [
        {
            'imagesrc': url1,
            'sentence': sentence1,
            'possibleTags': tags1
        },
        {
            'imagesrc': url2,
            'sentence': sentence2,
            'possibleTags': tags2
        },
        {
            'imagesrc': url3,
            'sentence': sentence3,
            'possibleTags': tags3
        }
    ];
    var channelObject = {};
    channelObject[channel] = channelData;
    var postData = {
        'password': password,
        'date': date,
        'data': channelObject
    };
    var request = new XMLHttpRequest();
    request.open(method, url, async);
    request.onreadystatechange = function () {
    // this callback will be called too many times at different states like: OPENED, HEARERS_RECEIVED,LOADING ,DONE
        if (request.readyState != 4 || request.status != 200) return;
        if (request.readyState == XMLHttpRequest.DONE) {
            alert("Success: " + request.responseText);
        }
    }

    // onload method is pretty much like Jquery's success method
    request.onload = function (e) {
        alert("Saved? : " + e.target.statusText);
    }
    request.setRequestHeader("Content-Type", "application/json");
    request.send(JSON.stringify(postData));
}
function refreshData(){
	var url = "https://englisheveryday.mybluemix.net/learningitem/refresh";
	var method = "POST";
    var async = true;
    var request = new XMLHttpRequest();
    request.open(method, url, async);
    request.onreadystatechange = function () {
    // this callback will be called too many times at different states like: OPENED, HEARERS_RECEIVED,LOADING ,DONE
        if (request.readyState != 4 || request.status != 200) return;
        if (request.readyState == XMLHttpRequest.DONE) {
            alert("Success: " + request.responseText);
        }
    }
    var password = document.getElementById('key').value;
    request.setRequestHeader("Content-Type", "application/json");
    request.send(JSON.stringify({'password':password}));
}